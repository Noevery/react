用于构建用户界面的 JavaScript 库

Facebook 开源 2011 Jordan Walke

操作 DOM 呈现页面

命令式编程

声明式编程

组件化模式 声明式编码

虚拟 DOM+优秀的 Diffing 算法

React Native

        /**
         * 虚拟DOM
         * 1.本质就是Object
         *
         * 对比之下虚拟DOM很轻量
        */

使用 jsx 创建虚拟 dom
jsx

/\*\*
_ jsx 语法规则
_ 定义虚拟 DOM，不要写引号
_ 标签中混入 JS 表达式时要用{}
_ 样式的类名指定不要用 class 要用 className
_ 内联样式，要用 style={{key:value}}
_ 虚拟 DOM 不支持多根标签
_ 标签必须闭合
_ 标签首字母
_ (1)小写字母开头，会转换 html 为同名元素,没有会报错
_ (2)大写字母开头，react 就去渲染对应的组件，若组件没有定义，则错误 \*
\*/


模块化              组件化             工程化




函数式组件   类式组件
